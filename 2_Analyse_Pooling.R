#!/bin/Rscript

###### HEADER - 2_Rx1d_NWEU_Pooling.R #######################################
# Author:  Joel Zeder (joel.zeder@env.ethz.ch)
# Date:    16.05.2023
# Purpose: Script that evaluates the pros and cons of pooling data spatially
#          for a better estimation of 100-year extreme precipitation return 
#          levels.
##############################################################################

rm(list=ls())
project_path  <<- "<your_project_path>"
plot_path     <<- file.path(project_path,"output/plots/2_NWEU_Pooling/")
data_path     <<- file.path(project_path,"output/data/2_NWEU_Pooling/")
dir.create(plot_path, showWarnings = F, recursive = T)
dir.create(data_path, showWarnings = F, recursive = T)
setwd(project_path)

#### LOAD LIBRARIES ##########################################################
library(tidyverse)
library(pammtools)
library(ncdf4)
library(ncdf4.helpers)
library(tidync)
library(broom)

library(dplyr)
library(ggplot2)
library(gtable)
library(data.table)
library(sp)
library(mgcv)
library(scico)
library(extRemes)
library(parallel)
library(scales)
library(geosphere)
library(RColorBrewer)
library(trend)
library(ggpubr)
library(evgam)
library(tools)
library(ggrepel)
library(png)
library(grid)

#### FUNCTIONS ##############################################################
source("0_settings_PLOTS.R")
source("_FUN_BasicPlots.R")
source("_FUN_GEVfitting.R")
source("_FUN_GEVfitting_ChFrei.R")
source("_FUN_WaittimeAnalysis.R")
source("_FUN_NWEU_Pooling.R")
source("_FUN_DataReader.R")
source("extRemes_new.R")  ## <-- for this you need to cope the scripts from 
                          ##     https://git.iac.ethz.ch/climphys/climate-extremes/regextremes
                          ##     to the same folder.

#### SETTINGS ###############################################################
options(dplyr.summarise.inform = FALSE)

exper_name <<- "Rx1d_NWEU_rlvl100_BS5"

years_preind  <- c(1851,1920)
years_current <- c(1951,2020)
years_future  <- c(2001,2070)

labels_esttype <- c(gp="Grid point",spat="Spat. varying",loc="Spat. constant",
                    pool="Data Pooled", gp_coup="Grid point (coupled)")
col_esttype <- c(gp="#ff4800",spat="#ff9700",loc="#ff9700",
                    pool="gold1", gp_coup="#ff4800")
labels_n100yr  <- c(`0`="#100yr events: 0",`1`="#100yr events: 1",`>=2`="#100yr events: >1")
labels_periods <- c(preind="Pre-industrial",current="Current climate",
                    future="Future climate", with100yr="With 100yr event",
                    without100yr="Without 100yr event",
                    CESM2LE="CESM2 LE",UKCP025="UKCP18")

#### BODY ###################################################################
## Which dataset should be analysed (LE or piControl run)?
dataset_sel <- "CESM2LE" 
data_path_out <<- file.path(data_path, dataset_sel)
dir.create(data_path_out, showWarnings = F, recursive = T)

## Read estimates and
load(file="CESM2LE_NWEU_Rx1d_gevfit_data.RData")
z_data_CESM2LE <- z_sel_gev_seq_df %>%
  left_join(read_GMST_data(dataset_sel), by="year") %>%
  mutate(dataset=dataset_sel) %>% 
  left_join(select(gp_gev_df,gp,year,loc,scl,shp),by=c("gp","year"))
rm(gp_gev_df, z_sel_gev_seq_df)

## Make evaluation methods plot:
make_eval_plot(z_data_CESM2LE)

## Get GEV fits for single grid point / pooled data:
z_data_CESM2LE_preind <- z_data_CESM2LE %>%
  filter(between(year,years_preind[1],years_preind[2]+1),
  ) %>% 
  get_record_type() %>% mutate(per="preind")
ens_gev_preind_df <- z_data_CESM2LE_preind %>%
  select(zval, gp, year, ens, lon, lat, GMST_smooth_ANO) %>%
  get_gev_pool_fit(ncores=60, overwrite=F, bs=1, verbose=T,
                   year_fit_range=years_preind,
                   year_eval=years_preind[2]+1, nonstat=F) %>%
  mutate(per="preind", dataset=dataset_sel)

z_data_CESM2LE_current <- z_data_CESM2LE %>%
  filter(between(year,years_current[1],years_current[2]+1)) %>% 
  get_record_type() %>% mutate(per="current")
ens_gev_current_df <- z_data_CESM2LE_current %>%
  select(zval, gp, year, ens, lon, lat, GMST_smooth_ANO) %>%
  get_gev_pool_fit(ncores=60, overwrite=F, bs=1, verbose=T,
                   year_fit_range=years_current,
                   year_eval=years_current[2]+1, nonstat=T) %>%
  mutate(per="current", dataset=dataset_sel)

z_data_CESM2LE_future <- z_data_CESM2LE %>%
  filter(between(year,years_future[1],years_future[2]+1)) %>% 
  get_record_type() %>% mutate(per="future")
ens_gev_future_df <- z_data_CESM2LE_future %>%
  select(zval, gp, year, ens, lon, lat, GMST_smooth_ANO) %>%
  get_gev_pool_fit(ncores=60, overwrite=F, bs=1, verbose=T,
                   year_fit_range=years_future,
                   year_eval=years_future[2]+1, nonstat=T) %>%
  mutate(per="future", dataset=dataset_sel)

## =============================================================================
## Make plot of Rx1d time series with 10 EPEs:
z_data_CESM2LE %>%
  filter(gp=="7.50_48.53", ens==52) %>%
  ggplot(aes(year)) +
  geom_line(aes(y=rlvl_100)) +
  geom_line(aes(y=zval), linewidth=0.3) +
  geom_point(aes(y=zval), size=2, col="white") +
  geom_point(aes(y=zval), size=0.7)

## =============================================================================
dataset_sel <- "UKCP025"
data_path_out <<- file.path(data_path, dataset_sel)
dir.create(data_path_out, showWarnings = F, recursive = T)

load(file="UKCP_NWEU_Rx1d_gevfit_data.RData")
gev_data_UKCP <- gp_gev_df

## Read GMST data:
z_data_UKCP <- z_sel_gev_seq_df %>% 
  left_join(read_GMST_data(dataset_sel), by="year") %>% 
  mutate(dataset=dataset_sel) %>% 
  select(-GMST_nonsmooth) %>% 
  left_join(select(gp_gev_df,gp,year,loc,scl,shp),by=c("gp","year"))
rm(gp_gev_df, z_sel_gev_seq_df)

## Get GEV fits for single grid point / pooled data:
z_data_UKCP_future <- z_data_UKCP %>% 
  filter(between(year,years_future[1],years_future[2]+1)) %>% 
  get_record_type() %>% mutate(per="future")
ens_gev_future_UKCP_df <- z_data_UKCP_future %>%
  select(zval, gp, year, ens, lon, lat, GMST_smooth_ANO) %>%
  get_gev_pool_fit(ncores=60, overwrite=F, bs=1, verbose=T,
                   year_fit_range=years_future,
                   year_eval=years_future[2]+1, nonstat=T, high_res_grid = F) %>%
  mutate(per="future", dataset=dataset_sel)

make_UKCP_grid_plot(z_data_UKCP)

## =============================================================================
z_data <- rbind(z_data_CESM2LE_preind, z_data_CESM2LE_current,
                z_data_CESM2LE_future, z_data_UKCP_future) %>% 
  rename(rlvl_100_ref=rlvl_100, loc_ref=loc, scl_ref=scl, shp_ref=shp) %>% 
  mutate(per=factor(per, c("preind","current","future"),ordered=T))

n100yr_df <- z_data %>% 
  group_by(per, ens, gp, dataset) %>% 
  summarise(n100yr=sum(!ltsel)) %>% 
  ungroup()

z_data %>% group_by(dataset, gp, per) %>%
  filter(between(year, min(year), min(year)+5) |
         between(year, max(year)-5, max(year)),.preserve = F) %>% 
  mutate(year_block=floor(year/20)) %>% 
  pull(year_block) %>% unique()

## =============================================================================
ens_gev_pool_df <- rbind(ens_gev_preind_df, ens_gev_current_df,
                         ens_gev_future_df, ens_gev_future_UKCP_df) %>%
  mutate( per=factor( per,c("preind","current","future"),ordered=T),
         type=factor(type,c("gp","gp_coup","spat","loc","pool"),ordered=T)) %>%
  left_join(z_data %>% select(year,gp,ens,rlvl_100_ref,loc_ref,scl_ref,shp_ref,
                              dataset,GMST_smooth_ANO,rec_type,per),
            by=c("year","gp","ens","dataset","per")) %>%
  left_join(n100yr_df, by=c("per","gp","ens","dataset")) %>%
  # rename(rlvl_100_ref=rlvl_100.y, rlvl_100=rlvl_100.x) %>%
  rename(loc=location, scl=scale, shp=shape) %>%
  mutate(loc_absdiff=loc-loc_ref, loc_reldiff=(loc-loc_ref)/loc_ref,
         scl_absdiff=scl-scl_ref, shp_absdiff=shp-shp_ref,
         rlvl_100_absdiff=rlvl_100-rlvl_100_ref,
         rlvl_100_reldiff=rlvl_100_absdiff/rlvl_100_ref,
         ci_coverage=ifelse(rlvl_100_ref<=rlvl_100_ciu95 & rlvl_100_ref>=rlvl_100_cil95,"within",NA),
         ci_coverage=if_else(rlvl_100_ref<rlvl_100_cil95,"below",ci_coverage),
         ci_coverage=if_else(rlvl_100_ref>rlvl_100_ciu95,"above",ci_coverage),
         ci_coverage=factor(ci_coverage, levels=c("below","within","above"), ordered=T),
         # rper_100=1-1/pevd_vec(rlvl_100_ref, location, scale, shape, type="GEV"),
         any100yr=if_else(n100yr>=1,"with100yr","without100yr"),
         ny100yr_grp=factor(if_else(n100yr>=2,">=2",as.character(n100yr)),c("0","1",">=2"), ordered=T),
         dataset=factor(dataset,c("CESM2LE","UKCP025"),ordered=T))


## =============================================================================
dataset_sel <- "CESM2LE"
per_sel     <- "current"
plot_df <- ens_gev_pool_df %>%
  filter(dataset==dataset_sel, per==per_sel, type %in% c("gp","loc")) %>% 
  mutate(rlvl_100_reldiff=if_else(rlvl_100_reldiff>0.6,0.6,rlvl_100_reldiff))
# range_x <- range(plot_df$loc_reldiff)
range_x <- range(plot_df$scl_absdiff)
range_y <- range(plot_df$shp_absdiff)
width_bp_x <- diff(range_y)/20; width_bp_y <- diff(range_x)/20
loc_bp_x   <- max(range_y)-diff(range_y)/25; loc_bp_y <- max(range_x)-diff(range_x)/25
col_range  <- c(-max(abs(plot_df$rlvl_100_reldiff)),
                 max(abs(plot_df$rlvl_100_reldiff)))

plot_freq_uncond_df <- plot_df %>% filter(type=="loc") %>% 
  group_by(type, ny100yr_grp) %>% 
  summarise(fraq_uncond=n()/nrow(.)) %>% 
  mutate(frac_uncond_str=sprintf("Uncond: %4.1f%%",fraq_uncond*100),
         x=max(range_x)-diff(range_x)/20, y=min(range_y)+diff(range_y)/10)
plot_freq_cond_df <- plot_df %>% filter(type=="loc", rec_type=="record") %>% 
  group_by(type, ny100yr_grp) %>% 
  summarise(fraq_cond=n()/nrow(.)) %>% 
  mutate(frac_cond_str=sprintf("  Cond: %4.1f%%",fraq_cond*100),
         x=max(range_x)-diff(range_x)/20, y=min(range_y)+diff(range_y)/20)

scl_shp_plot <- plot_df %>% 
  ggplot(aes(scl_absdiff,shp_absdiff)) +
  facet_grid(ny100yr_grp~type,
             labeller=as_labeller(c(labels_n100yr,labels_esttype))) +
  geom_point(col="grey70", size=0.9) +
  geom_point(aes(col=rlvl_100_reldiff), size=0.6) +
  geom_hline(yintercept=0, linetype="longdash", linewidth=0.4, col="grey30") +
  geom_vline(xintercept=0, linetype="longdash", linewidth=0.4, col="grey30") +
  geom_density_2d(bins=15, col="grey50", linewidth=0.3) +
  geom_boxplot(aes(x=loc_bp_y,y=shp_absdiff), fill="grey", outlier.size=0.05, width=width_bp_y) +
  geom_boxplot(aes(y=loc_bp_x,x=scl_absdiff), fill="grey", outlier.size=0.05, width=width_bp_x) +
  # geom_boxplot(aes(y=loc_bp_x,x=loc_reldiff), fill="grey", outlier.size=0.05, width=width_bp_x) +
  geom_text(data=plot_freq_uncond_df, aes(x=x,y=y,label=frac_uncond_str),
            family="Courier", fontface="bold", size=2.5, vjust=0, hjust=1) + #, col="#ff4800") +
  geom_text(data=  plot_freq_cond_df, aes(x=x,y=y,label=  frac_cond_str),
            family="Courier", fontface="bold", size=2.5, vjust=1, hjust=1) + #, col="#ff9700") +
  scale_color_gradient2(low=brewer.pal(9,"BrBG")[1],
                        high=brewer.pal(9,"BrBG")[9],
                        breaks=seq(-0.5,0.5,0.2), name=bquote(frac(widehat(z)[0.01]-z[ref],z[ref])),
                        labels=scales::percent, limits=col_range,
                        guide = my_triangle_colourbar(barheight = unit(5.,"cm"), barwidth=unit(0.4,"cm"),
                                                      direction="vertical", title.position="top",
                                                      label.position="left", title.hjust=0.5)) +
  # scale_x_continuous(name=bquote(widehat(mu)-mu[ref]), limits = range_x, labels=scales::percent) +
  scale_x_continuous(name=bquote(widehat(sigma)-sigma[ref]), limits = range_x) +
  scale_y_continuous(name=bquote(widehat(xi)-xi[ref]), limits = range_y) +
  coord_cartesian() +
  theme(legend.position = "left", legend.text = element_text(angle=-70))

base_plot <- ens_gev_pool_df %>%
  filter(dataset==dataset_sel, per==per_sel) %>% 
  filter(scl>0) %>%
  mutate(rper_100=1/(1-pevd_vec(rlvl_100_ref, loc, scl, shp, type="GEV"))) %>% 
  mutate(type_new=type, type="gp") %>% 
  ggplot(aes(type_new,rlvl_100_reldiff)) +
  facet_grid(ny100yr_grp~type, labeller=as_labeller(labels_n100yr)) +#, strip.position = "right") +  
  geom_vline(xintercept=c(2.5,4.5), linewidth=0.4, col="grey50") +
  scale_fill_manual(values=col_esttype, guide="none") +
  scale_x_discrete(name="Estimation method",
                   labels=gsub(" ","\n",labels_esttype)) +
  theme(legend.title = element_blank(), legend.position = "top",
        axis.text.x = element_text(angle=70,hjust=1,vjust=1))
rlvl_plot <- base_plot +
  geom_hline(yintercept=0, linetype="longdash", linewidth=0.4, col="grey30") +
  geom_boxplot(aes(group=type_new, fill=type_new), width=0.7, outlier.size=0.05,
               linewidth=0.5) +
  scale_y_continuous(labels=scales::percent,
                     name="100-year return level relative difference") +
  coord_cartesian(ylim=c(-0.25,0.25))
rper_plot <- base_plot +
  geom_hline(yintercept=100, linetype="longdash", linewidth=0.4, col="grey30") +
  geom_boxplot(aes(y=rper_100, group=type_new, fill=type_new), width=0.7, 
               outlier.size=0.05, linewidth=0.5) +
  scale_x_discrete(name="Estimation method",
                   labels=gsub(" ","\n",labels_esttype)) +
  scale_y_log10(name="Estiamted return period [yr]",breaks=c(50,100,200,500)) +
  coord_cartesian(ylim=c(45,550))

final_plot <- ggarrange(scl_shp_plot, rlvl_plot, rper_plot, align="h", nrow=1,
                        widths = c(0.6,0.2,0.2), labels = c("(a)","(b)","(c)"))
save_plot(final_plot, file.path(plot_path,paste("Pooling_res",dataset_sel,per_sel,#"loc",#"pool_nothighres",
                                                sep="_")),
          width=215, height=140, unit="mm")



ci_coverage_df <- ens_gev_pool_df %>%
  # filter(dataset==dataset_sel) %>% 
  filter(scl>0, type!="gp_coup") %>%
  group_by(ny100yr_grp, type, dataset, per) %>% 
  summarise(frac_above=sum(ci_coverage=="above", na.rm=T)/sum(!is.na(ci_coverage)),
            frac_below=sum(ci_coverage=="below", na.rm=T)/sum(!is.na(ci_coverage))) %>% 
  ungroup() %>% 
  mutate(ny100yr_grp_chr=as.character(ny100yr_grp))
ci_coverage_comb_df <- rbind(
  mutate(rename(select(ci_coverage_df,-frac_below),frac=frac_above),ci_type="above"),
  mutate(rename(select(ci_coverage_df,-frac_above),frac=frac_below),ci_type="below",frac=-frac)
)
ci_coverage_plot <- ci_coverage_comb_df %>% 
  ggplot() +
  facet_grid(ny100yr_grp~dataset+per, labeller=as_labeller(c(labels_n100yr, labels_periods)),
             scales = "free_y") +#, strip.position = "right") +  
  # geom_vline(xintercept=c(2.5,4.5), linewidth=0.4, col="grey50") +
  # geom_point(aes(x=type, y=frac)) +
  geom_bar(aes(x=type, y=frac,fill=type, group=ci_type), col="grey20",
           position="stack", stat="identity") +
  geom_hline(yintercept=0, linewidth=1, col="grey20") +
  # geom_hline(yintercept=c(-0.025,0.025), linewidth=0.6, col="white") +
  geom_hline(yintercept=c(-0.025,0.025), linewidth=0.6, col="grey50", linetype="longdash") +
  # scale_fill_manual(values=col_esttype, guide="none") +
  scale_x_discrete(name="Estimation method",
                   labels=gsub(" ","\n",labels_esttype)) +
  scale_y_continuous(labels = function(x) paste0(abs(x)*100,"%"),
                     name=bquote("Fraction of"~z[ref]~"below / above CI")) +
  scale_fill_manual(values=col_esttype, guide="none") +
  theme(legend.title = element_blank(), legend.position = "top",
        axis.text.x = element_text(angle=70,hjust=1,vjust=1))
save_plot(ci_coverage_plot, file.path(plot_path,paste("CIcoverage_res")),
          width=140, height=140, unit="mm")


