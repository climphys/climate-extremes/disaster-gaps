# README for disaster-gap project

1. File structure:
- Settings (mostly plotting) in "settings.R" file
- Retrieval of waiting times between 100-year events in "1a_Retrieve_WaitingTimes.R"
- Analysis of waiting time distribution in "1b_Analyse_WaitingTimes.R"
- Analysis of spatial pooling benefits in "2_Analyse_Pooling.R" (to run the respective code, make sure that you also copy the following scripts into the respository: https://git.iac.ethz.ch/climphys/climate-extremes/regextremes)

2. Data is available on https://doi.org/10.3929/ethz-b-000642891

File List: 
- README

- CESM2LE_metadata.RData
- CESM2LE_Rx1d_SREXsel_data.RData

- CESM2LE_WT_results.RData
- CESM2pictl_WT_results.RData

- CESM2LE_NWEU_Rx1d_gevfit_data.RData
- UKCP_NWEU_Rx1d_gevfit_data.RData

- IPCC-WGI-reference-regions-v4_R.rda

Content and elationship between files, if important: 
- CESM2LE_metadata.RData:
    Contains gridpoint metadata for CESM2LE data

- CESM2LE_Rx1d_SREXsel_data.RData: 
    - z_sel_gev_seq_df (data.frame): Contains Rx1d (zval) data for all
      100 ensemble members at each grid point in the WGI regions NEU, WCE,
      and MED of the CESM2 Large Ensemble (100 members). Further data, like
      estimated 50- and 100-year return level estimates are also provided.
    - gp_gev_df (data.frame): Contains output of GEV model fits at each grid
      point, such as parameter estimates (loc, scl, shp), as well as return
      level estimates (rlvl_50, rlvl_100, rlvl_500)

- CESM2LE_WT_results.RData / CESM12LE_WT_results.RData: 
    - seq_len_SREX_df (data.frame): Individual waiting times (seq_len) pooled
      for WGI regions (srex)
    - seq_len_sim_sel_quant_SREX_df (data.frame): Simulated waiting time
      frequencies (1%, 5%, 25%, 50%, 75%, 95%, 99% quantiles) pooled for 
      specific waiting time durations (seq_len)
 
- CESM2LE_NWEU_Rx1d_gevfit_data.RData / UKCP_NWEU_Rx1d_gevfit_data.RData:
    - same as CESM2LE_Rx1d_SREXsel_data.RData (see above), but restricted to
      the NWEU region (from the CESM2LE model and the UKCP18 model)

- IPCC-WGI-reference-regions-v4_R.rda:
    - IPCC WGI reference region geometries



3. Session Info:
```
> sessionInfo()
R version 4.2.2 (2022-10-31)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: openSUSE Leap 15.4

Matrix products: default
BLAS/LAPACK: /usr/local/OpenBLAS-0.3.21/lib/libopenblas_haswellp-r0.3.21.so

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C               LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8    LC_PAPER=en_US.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C             LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
 [1] grid      tools     parallel  stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] MittagLeffleR_0.4.1 png_0.1-8           ggrepel_0.9.2       evgam_1.0.0         ggpubr_0.5.0        trend_1.1.4        
 [7] RColorBrewer_1.1-3  geosphere_1.5-18    scales_1.2.1        extRemes_2.1-3      distillery_1.2-1    Lmoments_1.3-1     
[13] scico_1.3.1         mgcv_1.8-41         nlme_3.1-160        sp_1.5-1            data.table_1.14.6   broom_1.0.5        
[19] tidync_0.3.0        ncdf4.helpers_0.3-6 ncdf4_1.19          pammtools_0.5.8     forcats_0.5.2       stringr_1.5.0      
[25] dplyr_1.1.3         purrr_1.0.2         readr_2.1.3         tidyr_1.3.0         tibble_3.2.1        ggplot2_3.4.4      
[31] tidyverse_1.3.2    

loaded via a namespace (and not attached):
 [1] googledrive_2.0.0       colorspace_2.0-3        ggsignif_0.6.4          ellipsis_0.3.2          class_7.3-20           
 [6] fs_1.5.2                rstudioapi_0.15.0       proxy_0.4-27            listenv_0.8.0           farver_2.1.1           
[11] prodlim_2019.11.13      fansi_1.0.3             mvtnorm_1.1-3           lubridate_1.9.0         xml2_1.3.3             
[16] codetools_0.2-18        splines_4.2.2           Formula_1.2-4           jsonlite_1.8.3          dbplyr_2.2.1           
[21] compiler_4.2.2          httr_1.4.4              backports_1.4.1         assertthat_0.2.1        Matrix_1.5-3           
[26] lazyeval_0.2.2          gargle_1.2.1            cli_3.6.1               gtable_0.3.1            glue_1.6.2             
[31] rnaturalearthdata_0.1.0 Rcpp_1.0.10             carData_3.0-5           cellranger_1.1.0        RNetCDF_2.6-1          
[36] vctrs_0.6.4             iterators_1.0.14        globals_0.16.2          rvest_1.0.3             timechange_0.1.1       
[41] lifecycle_1.0.3         ncmeta_0.3.5            rstatix_0.7.1           googlesheets4_1.0.1     future_1.29.0          
[46] hms_1.1.2               stringi_1.7.8           foreach_1.5.2           e1071_1.7-12            checkmate_2.1.0        
[51] lava_1.7.0              rlang_1.1.1             pkgconfig_2.0.3         lattice_0.20-45         sf_1.0-9               
[56] labeling_0.4.2          tidyselect_1.2.0        parallelly_1.32.1       magrittr_2.0.3          R6_2.5.1               
[61] generics_0.1.3          DBI_1.1.3               pillar_1.9.0            haven_2.5.1             withr_2.5.0            
[66] units_0.8-0             survival_3.4-0          abind_1.4-5             future.apply_1.10.0     modelr_0.1.10          
[71] crayon_1.5.2            car_3.1-1               KernSmooth_2.23-20      utf8_1.2.2              tzdb_0.3.0             
[76] timereg_2.0.4           rnaturalearth_0.3.2     readxl_1.4.1            reprex_2.0.2            digest_0.6.30          
[81] classInt_0.4-8          extraDistr_1.9.1        numDeriv_2016.8-1.1     pec_2022.05.04          munsell_0.5.0     
```
